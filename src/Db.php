<?php

namespace Erc\CodeIgniter;

use Erc\CodeIgniter\App;

class Db {
	/**
	 * База данных
	 * @var string
	 */
	static protected $db = null;

	/**
	 * Установка базы данных
	 * @params string $db
	 */
	public static function setDb($db) {
		static::$db = $db;
	}

	/**
	 * Создание через Query Builder Limit
	 * @param array $params
	 * $params = array(
	 * 	'limit' int,
	 * 	'page' int
	 * )
	 */
	public static function limit($params = array()) {
		if (isset($params['limit'])) {
			$limit = (int) $params['limit'];

			if (isset($params['page']) && $params['page'] >= 0) {
				$offset = $limit * (int) $params['page'] - $limit;
				App::db(static::$db)->limit($limit, $offset);
			} else {
				App::db(static::$db)->limit($limit);
			}
		}
	}

	/**
	 * Создание через Query Builder Order By
	 * @param array $order_array
	 * @param array $params
	 * @param string $order_default
	 * @param string $by_default
	 */
	public static function order_by($order_array = array(), $params = array(), $order_default = '', $by_default = 'asc') {
		$by_array = array('asc', 'desc');

		if (isset($params['order']) &&
			in_array($params['order'], $order_array)
		) {
			$order = $params['order'];
		} else {
			$order = $order_default;
		}

		if (isset($params['by']) &&
			in_array($params['by'], $by_array)
		) {
			$by = $params['by'];
		} else {
			$by = $by_default;
		}

		if ($order && $by) {
			App::db(static::$db)->order_by($order, $by);
		}
	}

	/**
	 * Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (in_array($field, $field_list)) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field . '`', $value);
				} else {
					App::db(static::$db)->where($field, $value);
				}
			}
		}
	}

	/**
	 * Поиск множественный: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_in($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where_in($table . '.`' . $field_list[$field] . '`', $value);
				} else {
					App::db(static::$db)->where_in($field_list[$field], $value);
				}
			}
		}
	}

	/**
	 * Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_not($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field_list[$field] . '`' . ' != ', $value);
				} else {
					App::db(static::$db)->where($field_list[$field] . ' != ', $value);
				}
			}
		}
	}

	/**
	 * Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_not_in($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where_not_in($table . '.`' . $field_list[$field] . '`', $value);
				} else {
					App::db(static::$db)->where_not_in($field_list[$field], $value);
				}
			}
		}
	}

	/**
	 * Поиск подстроки: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function like($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->like($table . '.`' . $field_list[$field] . '`', $value);
				} else {
					App::db(static::$db)->like($field_list[$field], $value);
				}
			}
		}
	}

	/**
	 * Поиск подстроки: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_greater($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field_list[$field] . '`' . ' > ', $value);
				} else {
					App::db(static::$db)->where($field_list[$field] . ' > ', $value);
				}
			}
		}
	}

	/**
	 * Поиск подстроки: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_greater_than_equal($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field_list[$field] . '`' . ' >= ', $value);
				} else {
					App::db(static::$db)->where($field_list[$field] . ' >= ', $value);
				}
			}
		}
	}

	/**
	 * Поиск подстроки: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_less($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field_list[$field] . '`' . ' < ', $value);
				} else {
					App::db(static::$db)->where($field_list[$field] . ' < ', $value);
				}
			}
		}
	}

	/**
	 * Поиск подстроки: Создание через Query Builder Where
	 * @param array $field_list
	 * @param array $data
	 * @param string $table
	 */
	public static function where_less_than_equal($field_list = array(), $data = array(), $table = '') {
		foreach ($data as $field => $value) {
			if (isset($field_list[$field])) {
				if ($table) {
					App::db(static::$db)->where($table . '.`' . $field_list[$field] . '`' . ' <= ', $value);
				} else {
					App::db(static::$db)->where($field_list[$field] . ' <= ', $value);
				}
			}
		}
	}

	/**
	 * Создание параметров для поиска на основе формы
	 * @param array $field_list Поля базы
	 * @param array $form Данные
	 * @return array
	 */
	public static function searchData($field_list = array(), $form = array()) {
		$result = array();

		foreach ($field_list as $field) {
			if (isset($form[$field]) && $form[$field] !== '') {
				$result[$field] = $form[$field];
			}
		}

		return $result;
	}

	/**
	 * Выполнение INSERT/UPDATE данных
	 * @param string $table Таблица
	 * @param array $data Массив данных
	 * @param string $primary_key Первичный ключ
	 */
	public static function insertUpdateQuery($table, $data = array(), $primary_key = '') {
		if (empty($data[$primary_key])) {
			$data[$primary_key] = self::insertQuery($table, $data, $primary_key);
		} else {
			$data[$primary_key] = self::updateQuery($table, $data, $primary_key);
		}

		return $data[$primary_key];
	}

	/**
	 * Выполнение INSERT данных
	 * @param string $table Таблица
	 * @param array $data Массив данных
	 * @param string $primary_key Первичный ключ
	 */
	public static function insertQuery($table, $data = array(), $primary_key = 'id') {
		if ($primary_key !== null) {
			$data[$primary_key] = null;
		}

		App::db(static::$db)->insert($table, $data);
		$data[$primary_key] = App::db(static::$db)->insert_id();
		return $data[$primary_key];
	}

	/**
	 * Выполнение UPDATE данных
	 * @param string $table Таблица
	 * @param array $data Массив данных
	 * @param string $primary_key Первичный ключ
	 */
	public static function updateQuery($table, $data = array(), $primary_key = 'id') {
		if (!isset($data[$primary_key])) {
			$e = new \ValidateException('Not primary key');
			$e->setData([
				'table' => $table,
				'data' => $data
			]);
			throw $e;
		}

		App::db(static::$db)->where($primary_key, $data[$primary_key]);
		App::db(static::$db)->update($table, $data);
		return $data[$primary_key];
	}

	/**
	 * Обработка формы для подготовки данных к операциям INSERT/UPDATE
	 * @param string $table
	 * @param string $primary_key
	 * @param mixed(string|int) $id
	 */
	public static function deleteData($table, $primary_key, $id) {
		App::db(static::$db)->delete($table, array($primary_key => $id));
	}

	/**
	 * Получение допустимых значений колонки
	 * @param string $table_name Таблица
	 * @param string $column_name Колонка
	 * @param array
	 */
	public static function getColumnValues($table_name, $column_name) {
		$query = "SHOW COLUMNS FROM `" . $table_name . "` LIKE '" . $column_name . "'";

		$enum_array = App::db(static::$db)->query($query)->row_array();

		if (!empty($enum_array)) {
			$enum_array = str_replace(array('set(', 'enum(', ')', "'"), '', $enum_array['Type']);
			$enum_array = explode(',', $enum_array);

			foreach ($enum_array as &$a) {
				$a = trim($a);
			}
		}

		return $enum_array;
	}

}
